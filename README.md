# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

git clone https://ms2307@bitbucket.org/ms2307/stroboskop.git

Naloga 6.2.3:
https://bitbucket.org/ms2307/stroboskop/commits/ea0fcbec14b9488eda9a9cb11bdc5affb5d0f7c9

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/ms2307/stroboskop/commits/5e0f44644feed14d3cd49a40ed6df832d43ace4d

Naloga 6.3.2:
https://bitbucket.org/ms2307/stroboskop/commits/d74685f83431ab7e470573e6676c57e72de034ce

Naloga 6.3.3:
https://bitbucket.org/ms2307/stroboskop/commits/50cab7604fd671576f18b52a236abd3c2d0e2656

Naloga 6.3.4:
https://bitbucket.org/ms2307/stroboskop/commits/8a740ec5b440ad8747111879c767f4cd8150f006

Naloga 6.3.5:

git checkout master
git pull origin master
git merge izgled
git push origin master

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/ms2307/stroboskop/commits/ea7c82eae1eb4370338db04a1001466341ff0b8d

Naloga 6.4.2:
https://bitbucket.org/ms2307/stroboskop/commits/9f1868e9eaac4ada745e71ac191f9bf906cfb550

Naloga 6.4.3:
https://bitbucket.org/ms2307/stroboskop/commits/eb37536e00bafb0bc89cdc32c6e2bd17b2aeb048

Naloga 6.4.4:
https://bitbucket.org/ms2307/stroboskop/commits/c351ca798569769d59e06982ab8964f5f5c953cd